package datos;

public class Libro {
	
	private String titulo, autor;
	private float precio;
	private int publicacion;
	
	public Libro(String titulo, String autor, float precio, int publicacion) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.precio = precio;
		this.publicacion = publicacion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(int publicacion) {
		this.publicacion = publicacion;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Libro) {
			Libro libro = (Libro)obj;
			if (titulo.equals(libro.getTitulo())
					&& autor.equals(libro.getAutor()) 
					&& precio == libro.getPrecio()
					&& publicacion == libro.getPublicacion()) {
				
				return true;
				
			}
		}
		return false;
	}
	
	

}
