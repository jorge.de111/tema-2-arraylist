package programacion;

import java.util.ArrayList;

import datos.Libro;

public class Biblioteca {

	private ArrayList<Libro> libros;

	/////////////////////////////////////////////////////////////////////////////
	/// Operaciones de la clase de teor'ia ///
	/////////////////////////////////////////////////////////////////////////////

	public Biblioteca() {
		libros = new ArrayList<Libro>();
	}

	public ArrayList<Libro> getLibros() {
		return libros;
	}

	public Biblioteca(Libro[] libros) {
		this.libros = new ArrayList<Libro>();
		for (Libro elemento : libros) {
			this.libros.add(elemento);
		}
	}

	/**
	 * Busca la primera ocurrencia del autor en la colecci'on.
	 * 
	 * @param autor Autor a buscar.
	 * @return La posici'on del autor en la colecci'on o -1 si el autor no est'a.
	 * @throws AutorNulo Si autor es null.
	 */
	public int buscarAutor(String autor) throws AutorNulo {
		if (autor == null) {
			throw new AutorNulo();
		} else {
			for (int i = 0; i < libros.size(); i++) {
				Libro libro = libros.get(i);
				if (libro.getAutor().equals(autor)) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Elimina el primer libro asociado a una anyo de publicaci'on
	 * 
	 * @param publicacion Anyo de publicaci'on del libro que se quiere borrar
	 * @return true si lo borrar o false si no lo encuentra
	 */
	public boolean eliminarLibro(int publicacion) {
		for (int i = 0; i < libros.size(); i++) {
			Libro libro = libros.get(i);
			if (libro.getPublicacion() == publicacion) {
				libros.remove(i);
				return true;
			}
		}
		return false;
	}

	/**
	 * Elimina todos los libros que tienen asociados una mismo anyo de publicacio'n
	 * 
	 * @param publicacion Anyo de publicaci'on de los libros que se quieren borrar
	 */
	public void eliminarLibros(int publicacion) {
		while (eliminarLibro(publicacion))
			;
	}

	/////////////////////////////////////////////////////////////////////////////
	/// Operaciones de la clase de pr'actica en el aula ///
	/////////////////////////////////////////////////////////////////////////////

	/**
	 * Aumenta el precio de los libros publicados después de un anyo de
	 * publicaci'on
	 * 
	 * @param publicacion Anyo de publicaci'on de los libros que incrementan el
	 *                    precio
	 * @param incremento  Valor en el que se incrementa el precio del libro
	 * @return N'umero de libros que han cambiado su precio
	 */
	public int aumentarPrecio(int publicacion, float incremento) {
		int libroscambiados = 0;
		for (int i = 0; i < libros.size(); i++) {
			Libro libro = libros.get(i);
			if (libro.getPublicacion() >= publicacion) {
				libro.setPrecio(libro.getPrecio() + incremento);
				libroscambiados++;
			}
		}
		return libroscambiados;
	}

	/**
	 * Dice si al menos existe un libro en la biblioteca que tenga un t'itulo y
	 * autor dado
	 * 
	 * @param titulo T'itulo del libro buscado
	 * @param autor  Autor del libro buscado
	 * @return true si lo encuentra o false si no hay al menos un libro con ese
	 *         autor y t'itulo
	 */
	public boolean existeLibro(String titulo, String autor) {
		for (int i = 0; i < libros.size(); i++) {
			Libro libro = libros.get(i);
			if (libro.getTitulo() == titulo && libro.getAutor() == autor) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Dice si todos los libros de una biblioteca est'an regitrados en nuestra
	 * biblioteca
	 * 
	 * @param biblioteca Lista de libros que se quieren buscar
	 * @return true si se encuentran todos los libros y false si alg'un libro
	 *         registrado en la biblioteca especificada no se encuentra en nuestra
	 *         biblioteca
	 */
	public boolean incluida(Biblioteca biblioteca) {

		for (int i = 0; i <= biblioteca.getLibros().size(); i++) {
			Libro libro = biblioteca.getLibros().get(i);
			if (existeLibro(libro.getTitulo(), libro.getAutor())) {
				return true;
			}else {	}
		}return false;
	}
}
