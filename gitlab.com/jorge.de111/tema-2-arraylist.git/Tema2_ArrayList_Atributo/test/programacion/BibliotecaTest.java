package programacion;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import datos.Libro;

public class BibliotecaTest {

	Libro[] misLibros;
	Biblioteca miBiblioteca;
	
	Libro[] librosTest;
	Biblioteca bibliotecaTest;

	@Before
	public void setUp() throws Exception {
		misLibros = new Libro[]	{	new Libro("Libro de Juan", "Juan", 400.5f, 1950), 
									new Libro("Libro de Laura", "Laura", 1200f, 1988),
									new Libro("Libro de Pedro", "Pedro", 1500f, 1990)
								};
		miBiblioteca = new Biblioteca(misLibros);
		
		librosTest = new Libro[]	{	new Libro("Libro de Juan", "Juan", 400.5f, 1950), 
										new Libro("Libro de Laura", "Laura", 1200f, 1988),
										new Libro("Libro de Pedro", "Pedro", 1500f, 1990)};
		bibliotecaTest = new Biblioteca(librosTest);
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	///            Test de las operaciones de la clase de teor'ia                  ///
	//////////////////////////////////////////////////////////////////////////////////
	@Test
	public void testConstructorParametrizado() {
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(new Libro("Libro de Juan", "Juan", 400.5f, 1950), libros.get(0));
		assertEquals(new Libro("Libro de Laura", "Laura", 1200f, 1988), libros.get(1));
		assertEquals(new Libro("Libro de Pedro", "Pedro", 1500f, 1990), libros.get(2));	
	}

	
	@Test (expected = AutorNulo.class)
	public void testBuscarAutorAutorNulo() {
		miBiblioteca.buscarAutor(null);
	}
	
	@Test
	public void testBuscarAutorAutorEsta() {
		assertEquals(1, miBiblioteca.buscarAutor("Laura"));
	}
	
	@Test
	public void testBuscarAutorAutorNoEsta() {
		assertEquals(-1, miBiblioteca.buscarAutor("Marta"));
	}
	
	@Test
	public void testBuscarAutorAutorEstaMasVeces() {
		Libro[] misLibros = {	new Libro("Libro de Juan", "Juan", 400.5f, 1970), 
								new Libro("Libro de Laura", "Laura", 1200f, 1989),
								new Libro("Otro Libro de Laura", "Laura", 1500f, 1980)
			};
		Biblioteca miBiblioteca = new Biblioteca(misLibros);
		assertEquals(1, miBiblioteca.buscarAutor("Laura"));
	}
	
	@Test
	public void testEliminarLibroPublicacionEsta1Vez() {
		assertTrue(miBiblioteca.eliminarLibro(1950));
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(2, libros.size());
		assertEquals(-1, libros.indexOf(new Libro("Libro de Juan", "Juan", 400.5f, 1950)));
	}
	
	@Test
	public void testEliminarLibroPublicacionEstaMas1Vez() {
		assertTrue(miBiblioteca.eliminarLibro(1950));
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(2, libros.size());
		assertEquals(-1, libros.indexOf(new Libro("Libro de Juan", "Juan", 400.5f, 1950)));
	}
	
	@Test
	public void testEliminarLibroPublicacionNoEsta() {
		assertFalse(miBiblioteca.eliminarLibro(1955));
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(3, libros.size());
	}
	
	@Test
	public void testEliminarLibrosPublicacionEsta1Vez() {
		miBiblioteca.eliminarLibros(1950);
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(2, libros.size());
		assertEquals(-1, libros.indexOf(new Libro("Libro de Juan", "Juan", 400.5f, 1950)));
	}
	
	
	@Test
	public void testEliminarLibrosPublicacionEstaMas1Vez() {
		Libro[] misLibros = {	new Libro("Libro de Juan", "Juan", 400.5f, 1950), 
								new Libro("Libro de Laura", "Laura", 1200f, 1950),
								new Libro("Libro de Pedro", "Pedro", 1500f, 1950)
							};
		Biblioteca miBiblioteca = new Biblioteca(misLibros);
		miBiblioteca.eliminarLibros(1950);
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(0, libros.size());
	}
	
	@Test
	public void testEliminarLibrosPublicacionEstaEnTodos() {
		Libro[] misLibros = {	new Libro("Libro de Juan", "Juan", 400.5f, 1950), 
								new Libro("Libro de Laura", "Laura", 1200f, 1950),
								new Libro("Libro de Pedro", "Pedro", 1500f, 1950)
							};
		Biblioteca miBiblioteca = new Biblioteca(misLibros);
		miBiblioteca.eliminarLibros(1950);
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(0, libros.size());
	}
	
	@Test
	public void testEliminarLibrosPublicacionNoEsta() {
		miBiblioteca.eliminarLibros(1955);
		ArrayList<Libro> libros = miBiblioteca.getLibros();
		assertEquals(3, libros.size());
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	///       Test de las operaciones de la clase de pr'actica en el aula          ///
	//////////////////////////////////////////////////////////////////////////////////
	
	@Test
	public void testAumentarPrecioNinguno() {
		assertEquals (0, miBiblioteca.aumentarPrecio(1995, 10f));
		assertEquals(bibliotecaTest.getLibros(), miBiblioteca.getLibros());
	}
	
	@Test
	public void testAumentarPrecioUnaPublicacion() {
		bibliotecaTest.getLibros().get(2).setPrecio(miBiblioteca.getLibros().get(2).getPrecio()+10f);
		assertEquals (1, miBiblioteca.aumentarPrecio(1990, 10f));
		assertEquals(bibliotecaTest.getLibros(), miBiblioteca.getLibros());
	}
	
	@Test
	public void testAumentarPrecioUnaPublicacionAnterior() {
		bibliotecaTest.getLibros().get(2).setPrecio(miBiblioteca.getLibros().get(2).getPrecio()+10f);
		assertEquals (1, miBiblioteca.aumentarPrecio(1989, 10f));
		assertEquals(bibliotecaTest.getLibros(), miBiblioteca.getLibros());
	}
	
	@Test
	public void testAumentarPrecioDosPublicaciones() {
		bibliotecaTest.getLibros().get(1).setPrecio(miBiblioteca.getLibros().get(1).getPrecio()+10f);
		bibliotecaTest.getLibros().get(2).setPrecio(miBiblioteca.getLibros().get(2).getPrecio()+10f);
		assertEquals (2, miBiblioteca.aumentarPrecio(1988, 10f));
		assertEquals(bibliotecaTest.getLibros(), miBiblioteca.getLibros());
	}
	
	@Test
	public void testAumentarPrecioTodasLasPublicaciones() {
		bibliotecaTest.getLibros().get(0).setPrecio(miBiblioteca.getLibros().get(0).getPrecio()+10f);
		bibliotecaTest.getLibros().get(1).setPrecio(miBiblioteca.getLibros().get(1).getPrecio()+10f);
		bibliotecaTest.getLibros().get(2).setPrecio(miBiblioteca.getLibros().get(2).getPrecio()+10f);
		assertEquals (3, miBiblioteca.aumentarPrecio(1950, 10f));
		assertEquals(bibliotecaTest.getLibros(), miBiblioteca.getLibros());
	}
	
	@Test
	public void testAumentarPrecioAnteriorTodasLasPublicaciones() {
		bibliotecaTest.getLibros().get(0).setPrecio(miBiblioteca.getLibros().get(0).getPrecio()+10f);
		bibliotecaTest.getLibros().get(1).setPrecio(miBiblioteca.getLibros().get(1).getPrecio()+10f);
		bibliotecaTest.getLibros().get(2).setPrecio(miBiblioteca.getLibros().get(2).getPrecio()+10f);
		assertEquals (3, miBiblioteca.aumentarPrecio(1949, 10f));
		assertEquals(bibliotecaTest.getLibros(), miBiblioteca.getLibros());
	}
	
	@Test
	public void testExisteLibroFalse() {
		assertFalse (miBiblioteca.existeLibro("xxx", "yyy"));
	}
	
	@Test
	public void testExisteLibroInicio() {
		assertTrue (miBiblioteca.existeLibro(misLibros[0].getTitulo(), misLibros[0].getAutor()));
	}
	
	@Test
	public void testExisteLibroMedio() {
		assertTrue (miBiblioteca.existeLibro(misLibros[1].getTitulo(), misLibros[1].getAutor()));
	}
	
	@Test
	public void testExisteLibroFinal() {
		assertTrue (miBiblioteca.existeLibro(misLibros[2].getTitulo(), misLibros[2].getAutor()));
	}
	
	@Test
	public void testIncluidaVacia() {
		assertTrue (miBiblioteca.incluida(new Biblioteca(new Libro[0])));
	}
	
	@Test
	public void testIncluidaIdentica() {
		assertTrue (miBiblioteca.incluida(bibliotecaTest));
	}
	
	@Test
	public void testIncluidaSoloLibroIncio() {
		assertTrue (miBiblioteca.incluida(new Biblioteca(new Libro[]{librosTest[0]})));
	}
	
	@Test
	public void testIncluidaSoloLibroMedio() {
		assertTrue (miBiblioteca.incluida(new Biblioteca(new Libro[]{librosTest[1]})));
	}
	
	@Test
	public void testIncluidaSoloLibroFinal() {
		assertTrue (miBiblioteca.incluida(new Biblioteca(new Libro[]{librosTest[2]})));
	}
	
	@Test
	public void testIncluidaSoloDosLibros() {
		assertTrue (miBiblioteca.incluida(new Biblioteca(new Libro[]{librosTest[0], librosTest[2]})));
	}

	@Test
	public void testIncluidaFalseTodos() {
		bibliotecaTest.getLibros().get(0).setAutor("xxx");
		bibliotecaTest.getLibros().get(1).setAutor("yyy");
		bibliotecaTest.getLibros().get(2).setAutor("zzz");
		assertFalse (miBiblioteca.incluida(bibliotecaTest));
	}
	
	@Test
	public void testIncluidaFalseDos() {
		bibliotecaTest.getLibros().get(0).setAutor("xxx");
		bibliotecaTest.getLibros().get(2).setAutor("zzz");
		assertFalse (miBiblioteca.incluida(bibliotecaTest));
	}
	
	@Test
	public void testIncluidaFalseUno() {
		bibliotecaTest.getLibros().get(0).setAutor("xxx");
		assertFalse (miBiblioteca.incluida(bibliotecaTest));
	}

}
